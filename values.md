# Values

Our group strives to apply the following values. 

Community
: Understand the needs of communities for whom we build tools and foster sustainable, collaborative user and development communities with inclusive culture.

Open
: Build in the open using best-practices for reproducibility and engage potential stakeholders early.
  Don't stress about being "scooped" since that can serve only to reduce quality and efficiency of science in an era when human time is the most precious commodity; if our work is open, the source will be clear even if someone else's paper appears first.

Equity
: Who is impacted the products and social/organizational structures of a research portfolio?
  How do individual and group actions lead to inequitable distribution of resources and how can we compensate and organize to overcome systemic biases?

Balance
: Practice and foster a culture of setting boundaries, taking personal time, and declining new requests. Health and happiness come before research, and a clear head is necessary for creative work.

