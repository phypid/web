# Documentation

## Onboarding

Welcome to the PhyPID group! Group members often work remotely and/or asynchronously. We have adopted practices that facilitate collaboration and group planning while maintaining quality, reliability, and provenance. Your first steps will be logistical, and should be quick to set up.

Room access
: Send Jed your Buff card number so he can make sure you have room access. Most group members are in ECCS 1B11 in the engineering center. You can expect to get a 4k display, keyboard, and mouse. Please let Jed know if you would prefer a CU-owned laptop or workstation, or prefer to use your own.

Zulip
: We use a [group Zulip](https://phypid.zulipchat.com) to plan all sorts of technical and social activities, and for informal discussions and problem solving. Please ask Jed if you haven't already been invited. When you're ready, Jed will introduce you to the group on Zulip and in group meetings.

GitLab
: We use a [group GitLab](https://gitlab.com/phypid) for software products, papers, and all stages of technical collaboration. Your work (software, writing, slides) should generally be developed in a Git repository and you should expect to give access to collaborators. Please cretae a GitLab account if you haven't already and ask Jed to be added as a member of the group.

Noether
: Noether (`noether.colorado.edu`) is a shared server (2x 32-core processor with GPUs) that we use for development and testing as well as sharing data that is too large for Git repositories.
After Jed gives you access, you'll be able to log in with your CU IdentiKey and password (`ssh YourIdentikey@noether.colorado.edu`).
You can create a local ssh key (`ssh-keygen`) and upload it (`ssh-copy-id YourIdentikey@noether.colorado.edu`) for login convenience.

Web presence
: Prospective students, potential collaborators, and hiring committees will browse our group website or search for your name. When you start in the group (whether undergrad or grad), please add yourself to the [People](/people) page by editing the yaml file. Many fields are optional, but it's a good idea to create an [ORCID](https://orcid.org) account before your first publication. Whenever you're ready, and no less than one year before graduating, you should create a professional website. GitHub or GitLab Pages are good free options for use with static site generators like Sphinx (which is used to create this site) or Hugo (the hugo-academic theme is excellent).

## First steps

### Classes

If you are an incoming grad student, you should peruse the [classes for the coming semester](https://classes.colorado.edu) and ask more senior students for experience/recommendations regarding classes that may be of interest.
Plan to meet with Jed (can be virtual) some weeks in advance of each semester to discuss what classes will satisfy your curiosity and career aspirations, degree requirements, and prepare adequately for immediate needs in your research.

Our research generally spans several disciplines (computer science, applied math, X engineering, physics or other domain sciences) and it's likely that even if you are a CS major, you'll take a few classes from other departments.
It's important to become familiar with the terminology and culture enough to collaborate effectively with specialists in various domains.
Please recognize that relative to other students in such classes, you'll be picking up more of that culture and tacit knowledge on the fly, so try not to overload yourself and be prepared to ask for help (Zulip, meetings, and lab are good places, in addition to peers in your classes).
If you do not have formal background in CS, you can expect that experience when taking CS classes.
(Jed never took classes in CS, and other alumni and current members have had similar expeiences.)

### Contributing to software

For most new group members, your first technical contribution will be in the form of a merge request to a software repository.

#### Development environment

The first step will be to set up your development environment and run the test suite.
Please ask if you have questions about this for your operating system.
Most group members use one of:

* **Linux** native, with the development environment installed using a package manager. Arch and Debian/Ubuntu are the distributions we are most familiar with. Noether runs Debian.

* **OSX** with [Brew](https://brew.sh) to install compilers and other development tools. Apple has a horrible tendency to use heirloom versions of the GNU tools (before they adopted GPLv3) an Brew helps you use modern versions.

* **Windows with WSL**, which provides a Linux environment with shared file system. You can install graphical applications like VS Code (recommended IDE), Gmsh, and Paraview on the Windows side. The `\\wsl$` prefix can be used in a Windows file browser to access files on the "Linux" side. This tends to be more convenient than a virtual machine.

* **VS Code remote development** is excellent. So long as you have fast internet, you can develop on Noether with an environment that feels almost perfectly transparent. Visualization using Paraview will feel less responsive.

Regardless of operating system, if you don't have a strong preference otherwise, we encourage using VS Code with extensions such as `clangd` (for C/C++ code analysis), `rust-analyzer` (if you work on Rust), `Live Share` (for pair programming), `MyST-Markdown` (for editing docs), `Remote - SSH`/ `Remote - Containers`/`Remote - WSL` as appropriate, `Python`, and `Julia`.
Clangd needs to know the include flags for each compilation unit in the form of a file called `compile_commands.json`.
A universal way to create this is [Bear, the build ear](https://github.com/rizsotto/Bear), which is likely available in your package manager (e.g., `sudo apt install bear`), and can be run as `bear -- make -B` if the project is compiled using `make` (the `-B` flag tells `make` to build from scratch).

#### Merge requests

After installing the software and exploring tutorials, you should look for a contribution to make.
There might be issues tagged as good first contributions or you may get recommendations from a senior group member.
Since you'll have just put fresh eyes on the getting started and tutorial documentation, you might start with a contribution to improve that documentation.
No contribution is too small at this stage.

As you prepare your contribution, try to match the surrounding style.
For documentation, this will mean consistent notation and tense.
The project may have coding guidelines and auto-formatters such as clang-format for C/C++, cargo fmt for Rust, autopep8 or black for Python, etc.
Usually these can be run from the command-line, but editors like VS Code can run them automatically while typing or on save (see `Editor: Format On Save`).

Try to run the test suite or at least relevant parts while preparing your merge request.
Continuous integration (CI) will run when you make a merge request; this usually includes lints, test suites on multiple platforms/configurations, code coverage analysis, and documentation builds.
It's your responsibility to check the result and either fix the problems identified or ask for help if you don't understand what it reports or how to fix it.

You should strive to add tests (via test code or suitable run-time options) while writing each commit.
Ideally, each commit is as small as possible while being atomic and tested.
This ideal takes experience and can be challenging to achieve.
Commit messages should describe what is being added and why.
Try to look around at other merge requests and notice examples of nicely curated commits and commit messages.
Don't let the ideals get in the way of starting.

It will often be useful to push a draft version of your merge request before it's ready for review.
Prefix `Draft: ` to the merge request title to designate it as not ready.
Click into the file changes tab in your merge request and make a line comment with your question, tagging in people who might be able to answer (usually Jed and/or your colleagues with more experience).

You can expect multiple rounds of review, especially on your first merge requests.
When making changes in your branch, you'll likely use `git rebase -i` or similar functionality to amend the commits based on reviewer feedback, then force push your branch (and only your branch) when you're ready for further review.
If these tools are unfamiliar, you can just make new commits and request a "squash merge" (your whole merge request will become one commit upon merging).
Reviewer comments in the code will create threads and you can "resolve" those if you're confident the reviewer's comments were adequately addressed.
If you're unsure, please comment on what you changed and the reviewer can decide whether that resolves their concerns.
Remove the `Draft: ` prefix when you think the merge request is ready.
Each iteration may take some time for a reviewer to evaluate so you should always have a second project or research activity you can pursue while your merge request is waiting.
Eventually reviewers will approve your merge request and merge, 🎉 Yay! 🚀.

## Project planning

