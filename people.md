# People

:::{jinja} people
{% for category, cat_people in people.items() %}

## {{ category }}

{% for person in cat_people %}

### {{ person.name }} {% if person.orcid %} <a itemprop="sameAs" content="https://orcid.org/{{ person.orcid }}" href="https://orcid.org/{{ person.orcid }}" target="orcid.widget" rel="me noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon" /> {% endif %} {% if person.program %} | {{ person.program }} {% endif %}

{% if person.photo %}
```{image} {{ person.photo }}
:width: 200px
:alt: {{ person.name }}
:class: biopic
```
{% endif %}

{% if person.email %}
{{ person.email }}
{% endif %}

{% if person.pronouns %}
```{image} https://img.shields.io/static/v1?label=pronouns&message={{ person.pronouns }}&color=red&style=flat-square
:alt: Pronouns
```
{% endif %}

{% if person.scholar %}
```{image} https://img.shields.io/static/v1?label=&message=Google%20Scholar&color=gray&style=flat-square&logo=google-scholar
:alt: Google Scholar
:target: https://scholar.google.com/citations?user={{ person.scholar }}
```
{% endif %}

{% if person.github %}
```{image} https://img.shields.io/github/followers/{{ person.github }}?label=GitHub&logo=github&style=flat-square
:alt: GitHub Profile
:target: http://github.com/{{ person.github }}
```
{% endif %}

{% if person.gitlab %}
```{image} https://img.shields.io/badge/GitLab-{{ person.gitlab }}-108548?style=flat-square
:alt: GitLab Profile
:target: http://gitlab.com/{{ person.gitlab }}
```
{% endif %}

{% if person.twitter %}
```{image} https://img.shields.io/twitter/follow/{{ person.twitter }}?logo=twitter&style=flat-square
:alt: Twitter
:target: https://twitter.com/{{ person.twitter }}
```
{% endif %}


{% if person.website %}
```{image} https://img.shields.io/website?style=flat-square&url={{ person.website|urlencode }}
:alt: Website
:target: {{ person.website }}
```
{% endif %}


{{ person.bio }}


{% endfor %}
{% endfor %}
:::

:::{jinja} alumni
## Alumni

```{list-table}
:header-rows: 1

* - Name
  - Position
  - Current Position
{% for person in alumni %}
* - [{{ person.name }}]({{ person.website }}) {% if person.orcid %} <a itemprop="sameAs" content="https://orcid.org/{{ person.orcid }}" href="https://orcid.org/{{ person.orcid }}" target="orcid.widget" rel="me noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon" /> {% endif %}
  - {% if person.position %} {{ person.position }} {% endif %}
  - {% if person.current_position %} {{ person.current_position }} {% endif %}
{% endfor %}
```
