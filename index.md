# PhyPID Group

:::{toctree}
:maxdepth: 1
:hidden:

people
projects
software
openings
values
publications
doc/index
:::

## What we do

The Physical Prediction, Inference, and Design group conducts research on fast algorithms and robust community software for computational science and engineering.
Our home is in the [Computer Science Department](https://colorado.edu/cs) at [CU Boulder](https://colorado.edu), though our work is inherently interdisciplinary and we have group members from several different departments.

Physical
: Most of our work involves models of physical processes, often via partial differential equations and/or optics.
  Mathematical and physical modeling represents decades or centuries of domain knowledge; we invest in understanding such models and the associated culture sufficiently to work with domain scientists and engineers, and to recognize commonalities across multiple disciplines.
  
Prediction
: When initial and boundary conditions are provided, prediction is performed by solving a "forward model", which usually involves discretization in space and/or time.
  A forward model evaluation often requires implicitly solving systems of algebraic equations.
  We take a holistic approach, striving for methods that deliver Pareto optimality in objectives that are most relevant to end users, such as accuracy, human/compute cost, and total time to solution.
  Methods that achieve such optimality often depend on advances is multigrid methods, high-order structure-preserving discretization, efficient mapping to parallel CPU and GPU hardware, and development of high-quality community software.

Inference
: Natural phenomena often provide incomplete data, in which case we may use physical models and statistical methods to infer properties that are difficult or impractical to measure directly.
  We attempt to foresee inferential uses, to equip forward models with enabling technologies such as adjoint (gradient) models, and to develop and leverage technology that makes important types of inference tractable.
  
Design
: One of the greatest applications of models is to inform engineering and policy decisions through design optimization, experimental design, and quantitative risk assessment.
  We consider these applications and stakeholders throughout our development of computational infrastructure.
