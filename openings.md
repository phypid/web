# Openings

## [Summer Program for Undergraduate Research (SPUR)](https://www.colorado.edu/engineering/CU-SPUR-projects)

If you are an undergrad student at CU and interested in any of these projects, please email jed.brown@colorado.edu or apply directly with SPUR. The 2024 application deadline is March 17.

### [Safer scientific computing in Rust](https://www.colorado.edu/engineering/CU-SPUR-projects#safer_scientific_computing_in_rust-6690)

Rust is a new systems programming language that is rapidly growing in popularity, but library support for parallel and GPU-enabled scientific computing remains limited. As compared to the status quo languages of C, C++, and Fortran, use of Rust creates the opportunity to make scientific software more accessible, safe, reliable, easier to extend and maintain, and easier to package and distribute. The SPUR student will have an option of focusing on pragmatic use of Rust for scientific computing (comparing the experience to traditional languages) or fundamental primitives to ensure safety/invariant-preservation (such as dimensional consistency or parallel/GPU semantics). In both cases, the goal is to enable greater adoption of Rust and increased reliability of scientific libraries and applications.

### [Efficient structural analysis for soft robotics](https://www.colorado.edu/engineering/CU-SPUR-projects#efficient_structural_analysis_for_soft_robotics-6690)

Pneumatic-actuated soft robotics are an exciting low-cost, lightweight technology with a vast design space enabled by 3D printing. Design requires large-deformation simulation of thin-walled nearly-incompressible materials across a range of loading conditions. This regime is known to be challenging to ensure accuracy within widely-implemented variants of finite element analysis. Ratel is an open source finite element analysis package that employs new data structures and algorithms to improve accuracy and efficiency and to use GPUs. This project will study accuracy and efficiency of Ratel versus existing solvers such as FEBio (open source) and Abaqus (commercial) for a range of problems important to soft robotics. It will also conduct validation using experimental data.

### [Ratel-FreeCAD: integration and education in structural mechanics](https://www.colorado.edu/engineering/CU-SPUR-projects#ratel_freecad_integration_and_education_in_structural_mechanics-6690)

Computational simulations using finite element analysis (FEA) are an important aspect of everyday engineering. FreeCAD is an open-source 3D parametric modeling software package allowing engineers to design components with complex, real-world geometries. FreeCAD’s FEM workbench enables an integrated workflow, but only supports solvers based on classical methods, thus limiting efficiency, accuracy, capability to solve large problems, and use of GPUs. Alternatively, the user may export their FreeCAD models/meshes for use in external solver and analysis packages. This project will focus on creating a FreeCAD plugin for Ratel, an open-source solid mechanics library, which will allow for streamlined solid mechanics simulations capable of running at scale and on GPUs. This project will involve programming in Python and developing educational examples similar to those used in courses like MCEN 4173 and CVEN 4511. This work will use Ratel (which depends on libCEED and PETSc), Python, and FreeCAD.

## Research Assistant

The PhyPID group routinely has possible software engineering projects that would support research goals and impact, but may not have risen to the critical path for specific research. These projects typically require learning some about the problem domain and then developing code to integrate different packages or to present results.

To inquire about openings, please send an introductory email and CV to jed.brown@colorado.edu.
