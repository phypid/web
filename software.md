# Software

We believe that the value of computational research goes beyond proof of concept to its use in real-world problems.
This value is often most effectively captured by development of community software, which serves equally to advance the impact and visibility of methods research and to provide the feedback necessary to refine methods and define the next challenge.
On this page, you can find some of the software products that we maintain and/or actively contribute to.

## Community software libraries

### [PETSc](https://petsc.org/main)

### [libCEED](https://libceed.readthedocs.io)

### [TDyCore](https://tdycore.org)

### [CRIKit](https://crikit.science/documentation)

## Smaller projects

### [LFAToolkit.jl](https://github.com/jeremylt/LFAToolkit.jl/)

Local Fourier Analysis (LFA) is a mathematical technique for making sharp quantitative predictions of the convergence of iterative methods on homogeneous or quasi-periodic problems.
This is a modeling and optimization tool for using LFA to design and tune multigrid methods, especially for finite element discretizations and/or block/domain decomposition smoothers.

### [LigMG: Large Irregular Graph Multigrid](https://github.com/ligmg/ligmg)

Distributed-memory parallel multigrid solver for irregular graphs such as social networks.
LigMG uses a 2D partition of the sparse matrices and a multigrid method designed to expose parallelism while avoiding the growth in coarse grid complexity that occur in low-diameter sparse graphs.
