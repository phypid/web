# PhyPID Group site: https://phypid.org

[![Build Status](https://gitlab.com/phypid/web/badges/main/pipeline.svg)](https://gitlab.com/phypid/web/-/pipelines?page=1&scope=all&ref=main)
[![MIT License](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)](https://opensource.org/licenses/MIT)
[![CC BY 4.0](https://i.creativecommons.org/l/by/4.0/80x15.png)](https://creativecommons.org/licenses/by/4.0/)

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. `pip install -r requirements.txt`
1. `make html`
1. Open `_build/html/index.html` in a browser

If you install `pip install sphinx-autobuild` then you can run the following to monitor the project for saved files, automatically rebuild after changes, and autorefresh the browser.

```console
make autobuild
```

## License

The code in this repository is available under the [MIT License](LICENSE-MIT) and writing is available under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

## Acknowledgments

We are thankful to authors of all components that make this site possible, including the [PyData theme](https://pydata-sphinx-theme.readthedocs.io), [MyST Parser](https://myst-parser.readthedocs.io), and automation ideas adapted from Ryan Abernathey's [group site](https://ocean-transport.github.io).
